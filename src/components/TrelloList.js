import React from 'react'
import TrelloCard from './TrelloCard'

const TrelloList = ({title}) => (
  <div style={style.container}>
    <h2>{title}</h2>
    <TrelloCard/>
  </div>
)

const style = {
  container: {
    backgroundColor: "#ccc",
    borderRadius: 3,
    width: 300,
    padding: 8
  }
}


export default TrelloList
